module Cube where

import Prelude

import Control.Monad.Aff (Aff)
import Control.Monad.Eff.Console ( CONSOLE)
import Data.Array (mapWithIndex, (!!),snoc,drop)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Tuple (Tuple(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Math (cos, sin)
import Svg.Attributes as SA
import Svg.Elements as SE

-- Core Types
type Distance = Number

type Angle = Number

type Point2D =
  { x :: Distance
  , y :: Distance
  }

type Point3D =
  { x :: Distance
  , y :: Distance
  , z :: Distance
  }

type Edge = Tuple Int Int

type Shape =
  { vertices :: Array Point3D
  , edges :: Array Edge
  }

type Angle3D =
  { xa :: Angle       -- alpha 
  , ya :: Angle       -- beta
  , za :: Angle       -- gamma
  }

type AngVelocity3D = Angle3D -- velocity = angle/sec

type RotatingShape =
  { shape :: Shape
  , angVel :: AngVelocity3D
  , forward :: Boolean
  , speed :: Number
  }

data Axis = X | Y | Z

-- Model / State
type State = Array RotatingShape

-- Values

viewBoxSize :: Number
viewBoxSize = 600.0

viewCenter :: Point2D
viewCenter =
  { x: viewBoxSize / 2.0
  , y: viewBoxSize / 2.0
  }

frameRate :: Number
frameRate = 200.0

oneDegInRad :: Angle
oneDegInRad = 0.01745329255

tenDegInRad :: Angle
tenDegInRad = oneDegInRad * 10.0

accelerateBy :: Number -> Number
accelerateBy speedx = oneDegInRad * 50.0 * speedx

dampenPercent :: Number
dampenPercent = 1.0 - (0.9 / frameRate) -- 10% per second

initCube :: RotatingShape
initCube =
  { shape:
      { vertices:
          [ { x:  100.0, y:  100.0, z:  100.0 }
          , { x: -100.0, y:  100.0, z:  100.0 }
          , { x:  100.0, y: -100.0, z:  100.0 }
          , { x: -100.0, y: -100.0, z:  100.0 }
          , { x:  100.0, y:  100.0, z: -100.0 }
          , { x: -100.0, y:  100.0, z: -100.0 }
          , { x:  100.0, y: -100.0, z: -100.0 }
          , { x: -100.0, y: -100.0, z: -100.0 }
          ]
      , edges:
          [ Tuple 0 1
          , Tuple 0 2
          , Tuple 0 4
          , Tuple 1 5
          , Tuple 1 3
          , Tuple 2 3
          , Tuple 2 6
          , Tuple 4 5
          , Tuple 4 6
          , Tuple 3 7
          , Tuple 6 7
          , Tuple 5 7
          ]
      }
  , angVel:
      { xa: tenDegInRad
      , ya: tenDegInRad
      , za: tenDegInRad
      }
  , forward: true
  , speed: 1.0
  }

-- Events
data Query a
  = Tick a
  | IncAngVelocity Axis a
  | ChangDirec a
  | IncSpeed a
  | DecSpeed a
  | AddCube a
  | RemCube a

-------------------- UPDATE / REDUCERS --------------------

cubes :: forall eff. H.Component HH.HTML Query Unit Unit (Aff (console :: CONSOLE | eff))
cubes =
  H.component
    { initialState: const initialState
    , render
    , eval
    , receiver: const Nothing
    }
  
  where
    initialState :: State
    initialState = [initCube]

    render :: State -> H.ComponentHTML Query
    render state = HH.div[][ HH.ul[] $ map renderView state]  

    eval :: Query ~> H.ComponentDSL State Query Unit (Aff (console :: CONSOLE | eff))
    eval = case _ of
      Tick next -> do
        H.modify(
          \c -> map idling c
        )
        pure next

      IncAngVelocity axis next -> do
        H.modify
          (
            \c -> map ( changel axis ) c
          )
        pure next
      
      ChangDirec next ->  do
        H.modify
          (
            \c -> map chdir c
          )
        pure next
        
      IncSpeed next -> do
        H.modify
          (
            \c -> map incspeed c
              
          )
        pure next

      DecSpeed next -> do
        H.modify
          (
            \c -> map decspeed c
              
          )
        pure next
      AddCube next -> do
        H.modify
          (
            \c ->  snoc c initCube
          )
        pure next
      
      RemCube next -> do
        H.modify
          (
            \c -> drop 1 c
          )
        pure next

changel :: Axis -> RotatingShape -> RotatingShape
changel axis cube = do 
  let {xa, ya, za} = cube.angVel
  case axis of
    X -> cube { angVel { xa = if (cube.forward) then xa + accelerateBy cube.speed else xa - accelerateBy cube.speed } }
    Y -> cube { angVel { ya = if (cube.forward) then ya + accelerateBy cube.speed else ya - accelerateBy cube.speed } }
    Z -> cube { angVel { za = if (cube.forward) then za + accelerateBy cube.speed else za - accelerateBy cube.speed } }

chdir :: RotatingShape -> RotatingShape
chdir cube = do
  let direcval = cube.forward
  case direcval of
    true -> cube { forward = false }
    false -> cube { forward = true }
        
decspeed :: RotatingShape -> RotatingShape
decspeed cube = cube { speed = cube.speed * 0.5 }

incspeed :: RotatingShape -> RotatingShape
incspeed cube = cube{ speed = cube.speed+cube.speed *0.5 }

idling :: RotatingShape -> RotatingShape
idling cube = do
  let angVel = cube.angVel
      {vertices, edges} = cube.shape
      newShape =
        { edges: edges
        , vertices: rotateShape cube.forward vertices (anglePerFrame angVel)
        }
      newCube = cube
        { angVel = dampenAngVelocity angVel
        , shape = newShape
        }
  newCube

rotateShape :: Boolean -> Array Point3D -> AngVelocity3D -> Array Point3D
rotateShape direcval vertices ang =
  map (rotate direcval ang) vertices

rotate :: Boolean -> AngVelocity3D -> Point3D -> Point3D
rotate direcval { xa, ya, za } = rotateX xa >>> rotateY ya >>> rotateZ za
  where
    rotateX ang {x,y,z} = let Tuple ny nz = rotateInPlane direcval y z ang in { x, y:ny, z:nz }
    rotateY ang {x,y,z} = let Tuple nx nz = rotateInPlane direcval x z ang in { x:nx, y, z:nz }
    rotateZ ang {x,y,z} = let Tuple nx ny = rotateInPlane direcval x y ang in { x:nx, y:ny, z }
    
    rotateInPlane :: Boolean -> Number -> Number -> Number -> Tuple Number Number 
    rotateInPlane direcval axis1 axis2 ang = Tuple (axis1 * cos(ang) - axis2 * sin(ang)) (axis2 * cos(ang) + axis1 * sin(ang))
  
    
anglePerFrame :: AngVelocity3D -> Angle3D
anglePerFrame {xa, ya, za} =
  { xa: xa / frameRate
  , ya: ya / frameRate
  , za: za / frameRate
  }

dampenAngVelocity :: AngVelocity3D -> AngVelocity3D
dampenAngVelocity {xa, ya, za} =
    { xa: dampen xa
    , ya: dampen ya
    , za: dampen za
    }
  where
    dampen :: Number -> Number
    dampen ang = ang * dampenPercent -- Basics.max 0 (ang-drpf)

-------------------- VIEW --------------------
renderView :: RotatingShape -> H.ComponentHTML Query
renderView state = let
    {vertices, edges} = state.shape
    vert2Ds = map project vertices
  in
    HH.div [] $
      [ renderButton "rotX++" (IncAngVelocity X)
      , renderButton "rotY++" (IncAngVelocity Y)
      , renderButton "rotZ++" (IncAngVelocity Z)
      , renderButton "change-dir" (ChangDirec)
      , renderButton "vel++"  (IncSpeed)
      , renderButton "vel--" (DecSpeed)
      , renderButton "add-Cube" (AddCube)
      , renderButton "rem-Cube" (RemCube)
      ]
      <>
      [ SE.svg
        [ SA.viewBox 0.0 0.0 viewBoxSize viewBoxSize ]
        [ SE.g []
          (drawCube edges vert2Ds)
        ]
      ]
  where
    renderButton label query =
      HH.button
        [ HP.title label
        , HE.onClick (HE.input_ query)
        ]
        [ HH.text label ]

    -- parallel projection
    project :: Point3D -> Point2D
    project p =
      { x: p.x + viewCenter.x
      , y: p.y + viewCenter.y
      }

    drawCube :: Array Edge -> Array Point2D -> Array (H.ComponentHTML Query)
    drawCube edges vert2Ds =
      drawEdges edges vert2Ds <> drawVertices vert2Ds

    drawEdges :: Array Edge -> Array Point2D -> Array (H.ComponentHTML Query)
    drawEdges edges verts = let
        connectedVerts = map (\(Tuple v1 v2) -> Tuple (verts !! v1) (verts !! v2)) edges
      in
        map (\(Tuple v1 v2) -> drawLine (getPoint v1) (getPoint v2)) connectedVerts

    getPoint :: Maybe Point2D -> Point2D
    getPoint maybePoint = let
       default = { x: 100.0, y: 100.0 }
      in
        fromMaybe default maybePoint

    drawLine :: Point2D -> Point2D -> H.ComponentHTML Query
    drawLine a b =
      SE.path
        [ SA.d
          [ SA.Abs (SA.M a.x a.y)
          , SA.Abs (SA.L b.x b.y)
          ]
        , SA.stroke $ Just (SA.RGB 50 50 50)
        ]

    drawVertices :: Array Point2D -> Array (H.ComponentHTML Query)
    drawVertices vert2Ds =
      mapWithIndex drawVertex vert2Ds

    drawVertex :: Int -> Point2D -> H.ComponentHTML Query
    drawVertex idx {x, y} = SE.g []
      [ SE.text
          [ SA.x $ x + 5.0
          , SA.y $ y - 5.0
          , SA.fill $ Just (SA.RGB 150 150 150)
          ]
          [ HH.text $ show idx ]
      , SE.circle
          [ SA.r 3.0
          , SA.cx x
          , SA.cy y
          , SA.fill $ Just (SA.RGB 100 100 100)
          ]
      ]
